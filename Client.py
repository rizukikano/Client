import socket

# Membuat Objek Socket
sock = socket.socket()

# Definisi Port Tujuan
port = 8081

# Koneksi ke Server dengan error handling
try:
    sock.connect(('127.0.0.1', port))
    print("Connected with Server\n")
except Exception as e:
    print("Connection Refused")
    exit()


while True:

    # Kirim pesan ke Server dengan error handling
    dataSend = input("Input Message = ")
    try:
        sock.send(dataSend.encode())
        print("Message Sent\n")
    except Exception as e:
        print("\nSomething Wrong !!! ", e)
        sock.close()
        break
